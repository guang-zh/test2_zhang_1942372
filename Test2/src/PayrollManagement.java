/**
 * Test 2: Q2
 * PayrollManagement class uses SalariedEmployee, HourlyEmployee, UnionizedHourlyEmployee classes
 * Print out totalExpenses (yearly salaries) for all
 * @author guang
 *
 */
public class PayrollManagement {

	public static double getTotalExpenses(Employee[] input) {
		double totalExpenses=0;
		for (Employee e: input) {
			totalExpenses += e.getYearlyPay();
		}
		return totalExpenses;
	}

	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(5000);
		employees[1] = new HourlyEmployee(40, 13);
		employees[2] = new UnionizedHourlyEmployee(40, 11, 5000);
		employees[3] = new HourlyEmployee(35, 18);
		employees[4] = new UnionizedHourlyEmployee(35, 21, 6000);
		System.out.println("Total Expenses: "+ getTotalExpenses(employees));
	}
}
