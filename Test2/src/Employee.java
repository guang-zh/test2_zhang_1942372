/**
 * Test 2: Q2
 * Interface Employee with method getYearlyPay()
 * @author guang
 *
 */
public interface Employee {
	double getYearlyPay();

}
