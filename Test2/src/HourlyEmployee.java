/**
 * Test 2: Q2
 * HourlyEmployee class implements Employee calculating hours times hourly pay
 * @author guang
 *
 */
public class HourlyEmployee implements Employee {
	private double numHours;
	private double hourPay;
	
	public HourlyEmployee (double numHours, double hourPay) {
		this.numHours = numHours;
		this.hourPay = hourPay;
	}
	
	public double getYearlyPay() {
		return (this.numHours * this.hourPay * 52);
	}

}
