// Test 2: Q3

import java.util.Arrays;
import java.util.Comparator;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */

/**
 * Modified for adding the override equals() method
 * Override the hashCode() method
 * @author guang
 *
 */
public class Planet implements Comparable<Planet> {
	// Override equals() method to check name and order
	// First checking the type matches the Planet
	// @author guang
	// @param {Object} o
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Planet)) {
			return false;
		} 
		Planet p = (Planet) o;
		if (this.name==p.name && this.order==p.order) {
			return true;
		}
		return false;
	}
	
	// Override hashCode() method
	// @author guang
	public int hashCode() {
		return new Integer(this.name).hashCode();
	}
	
	// CompareTo method for Arrays.sort
	// @author guang
	public boolean compareTo(Planet p) {
		int pnameComparison=this.getName().compareTo(p.getName());
		if (pnameComparison==0) {
			return (this.planetarySystemName.equals(p.getPlanetarySystemName()));
		}
	}
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	
}
