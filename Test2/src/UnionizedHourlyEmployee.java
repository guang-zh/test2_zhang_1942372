/**
 * Test 2: Q2
 * UnionizedHourlyEmployee extends the HourlyEmployee
 * Add pensionContribution to the yearly salary
 * @author guang
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {
	private double pensionContribution;
	private double yearly;

	public UnionizedHourlyEmployee(double numHours, double hourPay, double pensionContribution ) {
		super(numHours, hourPay);
		this.yearly = super.getYearlyPay();
		this.pensionContribution = pensionContribution;
	}
	
	public double getYearlyPay() {
		return (this.yearly + this.pensionContribution);
	}

}
