import java.util.Arrays;
import java.util.Collection;

/**
 * Test 2: Q4
 * CollectionMethods for getting the first 3 inner planets of the system
 * @author guang
 *
 */
public class getInnerPlanets {

	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		int count=0;
		Collection<Planet> newP = new Collection<Planet>();
		for (Planet p: planets) {
			if (count<3) {
				newP.add(p);
				count++;
			}
		}	
	}

}
