/**
 * Test 2: Q2
 * SalariedEmployee stores and returns yearly salary
 * @author guang
 *
 */
public class SalariedEmployee implements Employee {
	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly = yearly;
	}
	
	public double getYearlyPay() {
		return this.yearly;
	}

}
